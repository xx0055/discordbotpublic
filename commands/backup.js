import { SlashCommandBuilder, PermissionFlagsBits } from 'discord.js';
import { existsSync, mkdirSync, writeFileSync, appendFileSync, createWriteStream } from 'node:fs';
import { get } from 'https';
import archiver from "archiver";

export const data = new SlashCommandBuilder()
    .setName('backup')
    .setDescription('Vytvoří zálohu tohoto kanálu')
    .setDMPermission(false)
    .addBooleanOption(option => option.setName('zip')
        .setDescription('Zazipuje ho s celou kategorií - řekni si mi o kopii')
        .setRequired(false))
    .setDefaultMemberPermissions(PermissionFlagsBits.ManageChannels);
export async function execute(interaction) {
    await interaction.reply("Připravuju zálohování...");
    var moreThan100 = true;
    const zip = interaction.options.getBoolean('zip');
    var velSouborOut = "";
    var lastID;
    var msgCount = 0;
    var attCount = 0;
    const channel = interaction.channel;
    var dir = "";
    if (channel.parent != null)
        dir = "./backups/" + channel.parent.name + "-" + channel.parent.id + "/";
    else
        dir = "./backups/" + channel.name + "-" + channel.id + "/";
    const channelBackupPath = dir + channel.name + "-" + channel.id + ".txt";
    if (!existsSync("./backups/")) {
        mkdirSync("./backups/");
    }
    if (!existsSync(dir)) {
        mkdirSync(dir);
    }
    if (!existsSync(dir + "/attachments")) {
        mkdirSync(dir + "/attachments");
    }
    if (!existsSync(channelBackupPath, "")) {
        writeFileSync(channelBackupPath, "");
    }
    await interaction.editReply("Spustilo se zálohování...").catch(err => console.log(err));
    while (moreThan100) {
        await channel.messages.fetch({ limit: 69, before: lastID }).then(async (messages) => {
            if (messages.size != 69)
                moreThan100 = false;
            //console.log(`Received ${messages.size} messages`);
            msgCount += messages.size;
            await interaction.editReply(":gear: Zálohuju zprávy :newspaper: \n" + msgCount + " zpráv(-a/-y) - " + attCount + " příloh(-a/-y)");
            if (messages.size > 0) {
                await messages.forEach(async (message) => {
                    appendFileSync(channelBackupPath, message.createdAt + " - " + message.author.username + "#" + message.author.discriminator + ": \"" + message.content + "\"");
                    if (message.attachments.size > 0) {
                        attCount++;
                        await interaction.editReply(":gear: Zálohuju přílohy :paperclip: \n" + msgCount + " zpráv(-a/-y) - " + attCount + " příloh(-a/-y)").catch(err => console.log(err));
                        await message.attachments.forEach(attachment => {
                            if (attachment.size < 8000000) {
                                const file = createWriteStream(dir + "/attachments/" + attachment.id + "-" + attachment.name);
                                const request = get(attachment.url, function (response) {
                                    response.pipe(file);
                                    file.on("finish", () => {
                                        file.close();
                                    });
                                });
                            }
                            appendFileSync(channelBackupPath, ", attachment at \"attachments/" + attachment.id + "-" + attachment.name);

                        });
                    }
                    lastID = message.id;
                    appendFileSync(channelBackupPath, "\n");
                });
            }
        });
    };
    if (zip) {
        await interaction.editReply(":gear: Vytvářím .zip soubor :package: \n" + msgCount + " zpráv(-a/-y) - " + attCount + " příloh(-a/-y)").catch(err => console.log(err));
        var fileName = "";
        if (channel.parent != null)
            fileName = channel.parent.name + "-" + channel.parent.id;
        else
            fileName = channel.name + "-" + channel.id;
        const dir = "./backups_zips/";
        const backupDir = "./backups/" + fileName + "/";
        if (!existsSync("./backups_zips/")) { mkdirSync("./backups_zips/"); }
        if (!existsSync(dir)) { mkdirSync(dir); }
        var output = createWriteStream(dir + fileName + ".zip");
        output.on('close', async () => {
            //console.log(archive.pointer() + ' total bytes');
            //console.log('Backup kategorie ' + fileName +" byl vytvořen.");
            var velSoubor = archive.pointer();
            if (velSoubor >= 1000 && velSoubor < 1000000)
                velSouborOut = (Math.round(velSoubor / 10) / 100) + " kB";
            else if (velSoubor >= 1000000)
                velSouborOut = (Math.round(velSoubor / 10000) / 100) + " MB";
            else
                velSouborOut = velSoubor + "b";
            await interaction.editReply(":confetti_ball: **Zálohování dokončeno!** :confetti_ball:\n" +
                msgCount + " zpráv(-a/-y) - " + attCount + " příloh(-a/-y)\n" +
                "Váš soubor má " + velSouborOut + "\n" +
                "Řekněte si správci o kopii").catch(err => console.log(err));
        });
        var archive = archiver('zip');
        archive.pipe(output);
        archive.directory(backupDir, false);
        await archive.finalize();
    } else {
        await interaction.editReply(":confetti_ball: **Zálohování dokončeno!** :confetti_ball:\n" +
            msgCount + " zpráv(-a/-y) - " + attCount + " příloh(-a/-y)\n" +
            "Řekněte si správci o kopii").catch(err => console.log(err));
    }
}
