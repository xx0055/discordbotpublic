import { SlashCommandBuilder, PermissionFlagsBits, ActionRowBuilder, StringSelectMenuBuilder } from 'discord.js';

export const data = new SlashCommandBuilder()
    .setName('group_toggle')
    .setDMPermission(false)
    .setDefaultMemberPermissions(PermissionFlagsBits.CreatePrivateThreads)
    .setDescription('Vyber si skupinu do které se chceš připojit');
export async function execute(interaction) {
    await interaction.reply({ content: 'Zde si vyber skupinu/y', components: [await getRow(interaction)], ephemeral: true });
}
export async function change_group(interaction) {
    const selected = interaction.values[0];

    if (selected === 'obed') {
        if (!await interaction.member.roles.resolve("1053380987964358708"))
            await interaction.member.roles.add("1053380987964358708");
        else
            await interaction.member.roles.remove("1053380987964358708");
    } else if (selected === 'jim') {
        if (!await interaction.member.roles.resolve("1053381007442714665"))
            await interaction.member.roles.add("1053381007442714665");
        else
            await interaction.member.roles.remove("1053381007442714665");
    } else if (selected === 'souteze') {
        if (!await interaction.member.roles.resolve("1053381004502511656"))
            await interaction.member.roles.add("1053381004502511656");
        else
            await interaction.member.roles.remove("1053381004502511656");
    } else if (selected === 'intr') {
        if (!await interaction.member.roles.resolve("1063217514395488438"))
            await interaction.member.roles.add("1063217514395488438");
        else
            await interaction.member.roles.remove("1063217514395488438");
    } else {
        await interaction.update('Someting went wrong lol');
        return;
    }
    await interaction.update({ components: [await getRow(interaction)] }).catch(err => console.log(err));
}

async function getRow(interaction) {
    var list = new StringSelectMenuBuilder()
        .setCustomId('cmd_group')
        .setPlaceholder('Nothing selected')
    if (!await interaction.member.roles.resolve("1053380987964358708")) list.addOptions(
        {
            label: '❌ Obědy',
            description: 'Budeš dostávat updaty o školní jídelně a později i beta přístup do lepšího iCanteen (;',
            value: 'obed',
        })
    else list.addOptions(
        {
            label: '✅ Obědy',
            description: 'Budeš dostávat updaty o školní jídelně a později i beta přístup do lepšího iCanteen (;',
            value: 'obed',
        })
    if (!await interaction.member.roles.resolve("1053381007442714665")) list.addOptions(
        {
            label: '❌ GYM',
            description: 'Pokud seš gymrat, tak si budeš s ostatníma tady rozumět',
            value: 'jim',
        })
    else list.addOptions(
        {
            label: '✅ GYM',
            description: 'Pokud seš gymrat, tak si budeš s ostatníma tady rozumět',
            value: 'jim',
        })
    if (!await interaction.member.roles.resolve("1053381004502511656")) list.addOptions(
        {
            label: '❌ Olympiády a Soutěže',
            description: 'Seš prostě nerd a zajmá tě kdy a jak toho budeš moct zneužít',
            value: 'souteze',
        })
    else list.addOptions(
        {
            label: '✅ Olympiády a Soutěže',
            description: 'Seš prostě nerd a zajmá tě kdy a jak toho budeš moct zneužít',
            value: 'souteze',
        })
    if (!await interaction.member.roles.resolve("1063217514395488438")) list.addOptions(
        {
            label: '❌ Intrák',
            description: 'Bydlíš na intru - lol',
            value: 'intr',
        })
    else list.addOptions(
        {
            label: '✅ Intrák',
            description: 'Bydlíš na intru - lol',
            value: 'intr',
        })
    const row = new ActionRowBuilder()
        .addComponents(
            list
        );
    return row;

}