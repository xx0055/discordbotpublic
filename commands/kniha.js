import { SlashCommandBuilder, PermissionFlagsBits, EmbedBuilder, ActionRowBuilder, StringSelectMenuBuilder } from 'discord.js';
import { writeFileSync } from 'node:fs';
import knihy from "../jsons/knihy.json" assert { type: "json" };
import { hledej } from '../funkce.js';

export const data = new SlashCommandBuilder()
    .setName('kniha')
    .setDescription('Managment tříd/skupin')
    .addStringOption(option => option.setName('co_chces')
        .setDescription('Chceš prodat, či koupit?')
        .setRequired(true)
        .addChoices(
            { name: 'Hledám učebnici', value: "koupim" },
            { name: 'Mám učebnici navíc', value: "prodam" },
            { name: "Chci odstranit svůj inzerát", value: "delete" }
        ))
    .addStringOption(option => option.setName('predmet')
        .setDescription('Zadej předmět knihy')
        .setRequired(false)
        .addChoices(
            { name: 'Matematika', value: "MAT" },
            { name: 'Fyzika', value: "FYZ" },
            { name: 'Ekonomie', value: "EKO" },
            { name: 'Angličtina', value: "AJ" },
            { name: 'Čeština', value: "CKJ" },
            { name: 'Chemie', value: "CHEM" },
            { name: 'Němčina', value: "NJ" },
            { name: 'Španělština', value: "SPJ" },
            { name: 'Francouština', value: "FRA" },
            { name: 'Člověk ve společnosti a dějinách', value: "CSD" },
            { name: 'Ruština', value: "RUS" },
            { name: 'Odborý předmět - Elektro', value: "ELO" },
            { name: 'Odborý předmět - Vývoj/Síťe', value: "VAS" },
            { name: 'Odborý předmět - Liceum', value: "LOL" },
            { name: "Jiné", value: "jine" }
        ))
    .addNumberOption(option => option.setName('rocnik')
        .setDescription('Ročník pro který je učebnice určena')
        .setRequired(false)
        .addChoices(
            { name: '1', value: 1 },
            { name: '2', value: 2 },
            { name: '3', value: 3 },
            { name: '4', value: 4 }
        ))
    .addStringOption(option => option.setName('nazev')
        .setDescription('Zadej název tvé knihy co hledáš/prodáváš')
        .setRequired(false))
    .addNumberOption(option => option.setName('kvalita')
        .setDescription('Ročník pro který je učebnice určena')
        .setRequired(false)
        .addChoices(
            { name: 'skoro nová', value: 1 },
            { name: 'trochu počmáraná', value: 2 },
            { name: 'vyplněná', value: 3 },
            { name: 'zničená (skoro)', value: 4 }
        ))
    .addNumberOption(option => option.setName('cena')
        .setDescription('Za kolik prodáš/koupíš v kč')
        .setRequired(false))
    .addAttachmentOption(option => option.setName("foto")
        .setDescription("Foto přední strany (jen při prodávání)")
        .setRequired(false))
    .setDMPermission(false)
    .setDefaultMemberPermissions(PermissionFlagsBits.Administrator);
export async function execute(interaction) {
    await interaction.reply({ content: "Pracuji na tom....", ephemeral: true });
    const pk = await interaction.options.getString('co_chces');
    const nazev = await interaction.options.getString('nazev');
    const rocnik = await interaction.options.getNumber('rocnik');
    const cena = await interaction.options.getNumber('cena');
    const foto = await interaction.options.getAttachment('foto');
    const kvalita = await interaction.options.getNumber("kvalita");
    const predmet = await interaction.options.getString("predmet");
    if (pk === "koupim") {
        if (!predmet) {
            await interaction.editReply("Musíš zadat alespoň předmět!");
            return;
        }
        var topka = [];
        knihy.forEach(async (inzerat) => {
            var kvalitka;
            switch (inzerat.kvalita) {
                case 1:
                    kvalitka = "Very good";
                    break;
                case 2:
                    kvalitka = "Good";
                    break;
                case 3: 
                    kvalitka = "Vyřešené";
                    break;
                case 4:
                    kvalitka = "V podstatě jen na podpal";
                    break;

                default:
                    kvalitka = "Něco se podělalo";
                    break;
            }

            await interaction.guild.members.fetch(inzerat.prodejce.replace(/[<>@]/g, "")).then(async (prodavacnik) => {
                const post = new EmbedBuilder()
                    .setColor(39423)
                    .setTitle(inzerat.nazev)
                    .setDescription("Cena: **" + inzerat.cena + "**kč\nKvalita: **" + kvalitka + "**")
                    .setImage(inzerat.foto)
                    .setFooter({ text: "Kontaktuj " + prodavacnik.displayName, iconURL: prodavacnik.avatar });
                var rocniktf;
                if (inzerat.rocnik - rocnik >= -1 && inzerat.rocnik - rocnik <= 1)
                    rocniktf = true;
                var nazevtf;
                if (nazev && await hledej(inzerat.nazev, nazev) > 70)
                    nazevtf = true;
                if ((rocniktf && inzerat.predmet === predmet) || (nazevtf && inzerat.predmet === predmet))
                    topka.push(post);
            });
            if (knihy.indexOf(inzerat) == knihy.length - 1)
                await interaction.editReply({ content: "Nalezeno toto:", embeds: topka });
        });
    } else if (pk === "prodam") {
        if (!nazev || !rocnik || !cena || !foto || !kvalita || !predmet) {
            await interaction.editReply("Pokud prodáváš, tak musíš vyplnit veškeré informace!");
            return;
        }
        if (!foto.contentType.includes("image")) {
            await interaction.editReply("Attachment musí být obrázek!");
            return;
        }
        var kniha = {
            nazev: nazev,
            rocnik: rocnik,
            cena: cena,
            foto: foto.attachment,
            kvalita: kvalita,
            predmet: predmet,
            prodejce: interaction.member.toString()
        };
        knihy.push(kniha);
        writeFileSync("./jsons/knihy.json", JSON.stringify(knihy));

        await interaction.editReply("Inzerát přidán!");
        update_zpravy(interaction);
    } else if (pk === "delete") {
        var list = new StringSelectMenuBuilder()
            .setCustomId('kniha')
            .setPlaceholder('Vyberte inzerác, který chcete vymazat');
        knihy.forEach(async (inzerat) => {

            if (inzerat.prodejce === interaction.member.toString() || interaction.member.toString() === "<@543459815746306055>")
                list.addOptions(
                    {
                        label: inzerat.nazev + " - " + (await interaction.guild.members.fetch(inzerat.prodejce.replace(/[<>@]/g, ""))).displayName,
                        description: 'Cena: ' + inzerat.cena + " Přemět: " + inzerat.predmet + " Ročník: " + inzerat.rocnik + "",
                        value: knihy.indexOf(inzerat).toString()
                    });
            if (knihy.indexOf(inzerat) == knihy.length - 1) {
                if (list.options[0])
                    await interaction.editReply({ content: "Zde máš", components: [new ActionRowBuilder().addComponents(list)] });
                else
                    await interaction.editReply({ content: "NIc tu nemáš", components: [] });
                update_zpravy(interaction);
            }
        });

    } else
        await interaction.editReply("Tvl jak se to stalo, reportni to pls (**/ticket**)");
}
export async function change_kniha(interaction) {
    splice(parseInt(interaction.values[0]), 1);
    writeFileSync("./jsons/knihy.json", JSON.stringify(knihy));
    var list = new StringSelectMenuBuilder()
        .setCustomId('cmd_kniha')
        .setPlaceholder('Vyberte inzerác, který chcete vymazat');
        knihy.forEach(inzerat => {

        if (inzerat.prodejce === interaction.member.toString() || interaction.member.toString() === "<@543459815746306055>")
            list.addOptions(
                {
                    label: inzerat.nazev,
                    description: 'Cena: ' + inzerat.cena + " Přemět: " + inzerat.predmet + " Ročník: " + inzerat.rocnik + "",
                    value: knihy.indexOf(inzerat).toString()
                });
    });
    if (list.options[0])
        await interaction.update({ content: "Zde máš", components: [new ActionRowBuilder().addComponents(list)] });
    else
        await interaction.update({ content: "Nic tu nemáš", components: [] });
    await update_zpravy(interaction);
}

async function update_zpravy(interaction) {
    var knihyChannel = await interaction.guild.channels.fetch("1055461552708386876");
    await knihyChannel.messages.fetch()
    var knihyMessage;
    await knihyChannel.messages.cache.forEach(async (message) => {
        if (message.content === "Nabídky učebnic:") {
            knihyMessage = message;
            return;
        }
    })
    if (!knihyMessage) {
        knihyMessage = await knihyChannel.send("Nabídky učebnic:")
    }
    var topka = [];
    knihy.forEach(inzerat => {
        var kvalitka;
        switch (inzerat.kvalita) {
            case 1:
                kvalitka = "Very good";
                break;
            case 2:
                kvalitka = "Good"
                break;
            case 3:
                kvalitka = "Vyřešené"
                break;
            case 4:
                kvalitka = "V podstatě jen na podpal"
                break;

            default:
                kvalitka = "Něco se podělalo"
                break;
        }

        interaction.guild.members.fetch(inzerat.prodejce.replace(/[<>@]/g, "")).then(async prodavacnik => {
            const post = new EmbedBuilder()
                .setColor(0x0099FF)
                .setTitle(inzerat.nazev)
                .setDescription("Cena: **" + inzerat.cena + "**kč\nKvalita: **" + kvalitka + "**\nRočník: **" + inzerat.rocnik + "**\nPředmět: **" + inzerat.predmet + "**")
                .setImage(inzerat.foto)
                .setFooter({ text: "Kontaktuj " + prodavacnik.displayName, iconURL: prodavacnik.avatar });
            topka.push(post);
        });
    })
    await knihyMessage.edit({ embeds: topka })
}