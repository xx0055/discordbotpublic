import { SlashCommandBuilder, PermissionFlagsBits } from 'discord.js';
import { createTransport } from 'nodemailer';
import config from '../jsons/config.json' assert { type: "json" };
import users from '../jsons/users.json' assert { type: "json" };
import { writeFileSync } from 'node:fs';
import studenti from "../jsons/studenti.json" assert { type: "json" };
import { prihlaseno } from "./verify.js";

export const data = new SlashCommandBuilder()
  .setName('login')
  .setDefaultMemberPermissions(PermissionFlagsBits.UseExternalStickers)
  .setDescription('Ověř se přes školní účet!')
  .addStringOption(option => option.setName('username')
    .setDescription('Zadej školní login do novellu')
    .setRequired(true))
  .setDMPermission(false);
export async function execute(interaction) {
  //await interaction.reply({ content: "Zařazuju tě do systému...", ephemeral: true });
  var name = await interaction.options.getString('username');
  if (await name.includes("@spseplzen.cz"))
    name = name.replace("@spseplzen.cz", "");
  if (await interaction.member.roles.cache.get("1045791758128255017")) {
    await interaction.reply({ content: "Hele už seš v systému.", ephemeral: true });
    return;
  }
  if (users[interaction.user.id]) {
    await interaction.reply({ content: "Kód byl poslán, počkej než vyprchá nebo piš na podporu! (**/ticket**)", ephemeral: true });
    return;
  }
  if (name.length >= 69) {
    await interaction.reply({ content: "Maximální délka řetězce je 69!", ephemeral: true });
    return;
  }
  if(name === "guest"){
    if(await interaction.member.roles.cache.get("1061938494399852596")){
      await interaction.reply({content: "Už jsi jako GUESt přihlášen", ephemeral: true})
    } else {
      await interaction.member.roles.add("1061938494399852596");
      await interaction.reply({content: "Úspěšně jsi se přihlásil jako GUEST", ephemeral: true})
    }
    return;
  }
  if (studenti[name] == undefined) {
    await interaction.reply({ content: "Seš sus - napsal jsi své jméno správně?", ephemeral: true });
    return;
  }
  if (studenti[name].discord && studenti[name].discord != await interaction.member.toString()) {
    await interaction.reply({ content: "Tato osoba je již zaregistrovaná... Pokud se tu někdo vydává za tebe, tak piš na podporu! (**/ticket**)", ephemeral: true }).catch(err => console.log(err));
    return;
  } else if (studenti[name].discord === await interaction.member.toString()) {
    await interaction.reply({ content: "Vítej zpět bobku! Vše připravujeme...", ephemeral: true }).catch(err => console.log(err));
    await prihlaseno(interaction,name);
    await interaction.editreply({ content: "Vítej zpět bobku! Vše je hotovo!!", ephemeral: true }).catch(err => console.log(err));
    return;
  } else {
    var email = name + "@spseplzen.cz";
    users[interaction.user.id] = Math.floor(Math.random() * 899999 + 100000);
    users[interaction.user.id + "name"] = name;
    var mailOptions = {
      from: 'spseverify@volny.cz',
      to: `${email}`,
      subject: 'Verifikace na Prestižní Discord!',
      text: `Tu máš kódík bobku: ${users[interaction.user.id]},
pokud jsi se nechtěl přihlásit na náš školní diskord tak tento email ignoruj!
A nebo se můžeš za náma přidat a tento kód využít ;)  https://discord.gg/WGEPjEAWKY`
    };
    createTransport({
      host: "smtp.volny.cz",
      port: 465,
      secure: true,
      auth: {
        user: 'spseverify@volny.cz',
        pass: `${config.email_pass}`
      }
    }).sendMail(mailOptions, async (error, info) => {
      if (error) {
        await interaction.reply({ content: "Email nebyl poslán, napiš na podporu! (**/ticket**)", ephemeral: true }).then(to()).catch(err => console.log(err));
        console.log(error);
      } else {
        console.log('Email sent: ' + info.response);
        await interaction.reply({ content: "Kód odeslán na tvůj školní email! Napište ho do \"/verify\"", ephemeral: true }).then(to()).catch(err => console.log(err));
      }
    });

    function to() {
      writeFileSync("./jsons/users.json", JSON.stringify(users));
      setTimeout(function () {
        if (users[interaction.user.id]) {
          delete users[interaction.user.id];
          delete users[interaction.user.id + "name"];
          writeFileSync("./jsons/users.json", JSON.stringify(users));
        }
      }, 6000000);
    }

  }
}

