import { SlashCommandBuilder, PermissionFlagsBits } from 'discord.js';
import { updateNovellChannel } from "../lib/updateNovellChannel.js";
import studenti from "../jsons/studenti.json" assert { type: "json" };
import vyucujici from "../jsons/vyucujici.json" assert { type: "json" };
import skupiny from "../jsons/skupiny.json" assert { type: "json" };
import changesNovell from "../jsons/changesNovell.json" assert { type: "json" };
import fs from "fs";
export const data = new SlashCommandBuilder()
    .setName('novell')
    .setDefaultMemberPermissions(PermissionFlagsBits.Administrator)
    .setDMPermission(false)
    .setDescription('noVELLÁCKÝ KOMADY')
    .addBooleanOption(option => option.setName("sync")
        .setDescription("Synchronizuje kontakty s Novellem (Vymaže vše bez OW)")
        .setRequired(false))
    .addBooleanOption(option => option.setName("fullscan")
        .setDescription("Zkontroluje všechny třídy žáků, kteří jsou na tomto serveru")
        .setRequired(false));
export async function execute(interaction) {
    const sync = await interaction.options.getBoolean("sync");
    const fullCheck = await interaction.options.getBoolean("fullscan");
    const novellChannel = await interaction.guild.channels.fetch("1065768354750668940");
    if(fullCheck && interaction.member.id !== "543459815746306055"){
        await interaction.reply({ content: "Alane, nebudeš spamovat , fuck you", ephemeral: true });
        console.log("Alan spam, jak to zkusil xddd");
        return;
    }
    if (sync) {
        await interaction.reply({ content: "Ukládám synchronizovaná data z novellu....", ephemeral: true });
        try {
            const keyss = await Object.keys(changesNovell.added.studenti);
            await keyss.forEach(async (student) => {
                studenti[student] = changesNovell.added.studenti[student];
            });
        } catch (error) {
        }
        try {
            const keysv = await Object.keys(changesNovell.added.ucitele);
            await keysv.forEach(async (vyuc) => {
                vyucujici[vyuc] = changesNovell.added.ucitele[vyuc];
            });
        } catch (error) {
        }
        try {
            const keysss = await Object.keys(changesNovell.added.tridy);
            await keysss.forEach(async (skupin) => {
                skupiny[skupin] = changesNovell.added.tridy[skupin];
            });
        } catch (error) {
        }

        changesNovell.removed.studenti.forEach(student => {
            if(!studenti[student].discord)delete studenti[student];
            else {
                
            }
        });
        changesNovell.removed.ucitele.forEach(vyuc => {
            vyucujici[vyuc].retired = true;
        });
        changesNovell.removed.tridy.forEach(skupin => {
            delete skupiny[skupin];
        });

        fs.writeFileSync("./jsons/studenti.json", JSON.stringify(studenti));
        fs.writeFileSync("./jsons/vyucujici.json", JSON.stringify(vyucujici));
        fs.writeFileSync("./jsons/skupiny.json", JSON.stringify(skupiny));
        await updateNovellChannel(novellChannel);
        await interaction.editReply({ content: "Uloženo!", ephemeral: true });
        return;
    }
    await interaction.reply({ content: "Refrešuji", ephemeral: true })

    await updateNovellChannel(novellChannel,fullCheck);
    await interaction.editReply({ content: "Done, refrešnuto", ephemeral: true })
}