import { SlashCommandBuilder, PermissionFlagsBits } from 'discord.js';

export const data = new SlashCommandBuilder()
	.setName('ping')
	.setDefaultMemberPermissions(PermissionFlagsBits.CreatePrivateThreads)
	.setDMPermission(false)
	.setDescription('Abys viděl jak shit servery máme xd');
export async function execute(interaction) {
	await interaction.reply({ content: "...", ephemeral: true }).catch(err => console.error(err));
	var ca = 0;
	await interaction.fetchReply().then(reply => ca = reply.createdTimestamp).catch(err => console.error(err));
	const ping = ca - interaction.createdAt;
	await interaction.editReply({ content: "Odezva je " + ping + "ms na naší straně a " + interaction.client.ws.ping + "ms na straně discordu", ephemeral: true }).catch(err => console.error(err));
}