import { SlashCommandBuilder, PermissionFlagsBits } from 'discord.js';

export const data = new SlashCommandBuilder()
	.setName('server')
	.setDefaultMemberPermissions(PermissionFlagsBits.CreatePrivateThreads)
	.setDMPermission(false)
	.setDescription('Poví ti něco o tomto serveru a botovi');
export async function execute(interaction) {
	// interaction.guild is the object representing the Guild in which the command was run
	const time = interaction.createdTimestamp - interaction.client.readyTimestamp;
	const date = new Date(time);
	await interaction.reply({
		content: `Toto je ${interaction.guild.emojis.cache.get('1049807116749254787')} **${interaction.guild.name}** ${interaction.guild.emojis.cache.get('1049807116749254787')} s **${(await interaction.guild.roles.fetch("1045791758128255017")).members.size}** přihlášenýma studentama. 
${interaction.guild.emojis.cache.get('1049450426694639616')} Běžíme už **${date.getUTCMonth()}M:${date.getUTCDate() - 1}D:${date.getUTCHours()}h:${date.getUTCMinutes()}m:${date.getUTCSeconds()}s** v kuse! Pog ${interaction.guild.emojis.cache.get('1049450426694639616')} `, ephemeral: true
	});
}      