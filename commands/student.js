import { ChannelType, SlashCommandBuilder, PermissionFlagsBits, ButtonStyle, ActionRowBuilder, StringSelectMenuBuilder, ButtonBuilder, TextInputStyle, EmbedBuilder, TextInputBuilder, ModalBuilder } from 'discord.js';
import studenti from '../jsons/studenti.json' assert { type: "json" };
import skupiny from "../jsons/skupiny.json" assert { type: "json" };
import vyucujici from "../jsons/vyucujici.json" assert { type: "json" };
import config from "../jsons/config.json" assert { type: "json" };
import got from "got";
import roles from "../jsons/roles.json" assert { type: "json" };
import { writeFileSync } from 'node:fs';
var tD = new TextDecoder()

export const data = new SlashCommandBuilder()
    .setName('student')
    .setDescription('Správa studentů')
    .setDMPermission(false)
    .setDefaultMemberPermissions(PermissionFlagsBits.Administrator)
    .addStringOption(option => option.setName('jmeno')
        .setDescription('Jméno studenta (Discord/Novell)')
        .setRequired(false))
    .addBooleanOption(option => option.setName("create")
        .setDescription('Create student')
        .setRequired(false))
    .addBooleanOption(option => option.setName("discord")
        .setDescription('Vyhledá studenty *(oběti)* které máme zde na tomto discord serveru')
        .setRequired(false))
    .addBooleanOption(option => option.setName("overwritten")
        .setDescription('Vyhledá studenty s/bez atributu "ow"')
        .setRequired(false))
    .addBooleanOption(option => option.setName("legit")
        .setDescription('Vyhledá studenty s/bez atributu "checked"')
        .setRequired(false))
    .addStringOption(option => option.setName('trida')
        .setDescription('Vyhledá studenty z dané třídy (jen zde na discordu)')
        .setRequired(false))
    .addStringOption(option => option.setName('zamereni')
        .setDescription('Vyhledá studenty z daného zaměření (jen zde na discordu)')
        .setRequired(false)
        .addChoices(
            { name: 'Elektro', value: "26-41-M/01 Elektrotechnika" },
            { name: 'Elektro - TMD', value: "26-41-M/01 Elektrotechnika/TMD" },
            { name: 'Elektro - IOT', value: "26-41-M/01 Elektrotechnika IOT" },
            { name: 'IT - VASS', value: "18-20-M/01 Informační technologie - VASS" },
            { name: 'IT - ITFP', value: "18-20-M/01 Informační technologie - ITFP" },
            { name: 'Liceum - LOL - Imagine', value: "78-42-M/01 Technické lyceum" }));
export async function execute(interaction) {
    const jmeno = await interaction.options.getString('jmeno');
    const create = await interaction.options.getBoolean('create');
    const tridaHled = await interaction.options.getString('trida');
    const discord = await interaction.options.getBoolean('discord');
    const zamereni = await interaction.options.getString('zamereni');
    const overwritten = await interaction.options.getBoolean('overwritten');
    const legit = await interaction.options.getBoolean('legit');


    //--------------------- Jen výpis ---------------------
    if (create == undefined && jmeno == undefined && tridaHled == undefined && discord == undefined && zamereni == undefined && overwritten == undefined && legit == undefined) {
        const keyss = Object.keys(studenti);
        var student;
        keyss.forEach(ss => {
            if (studenti[ss].discord === interaction.user.toString()) { student = ss; return; }
        });
        if (student) { await interaction.reply({ embeds: [await getEmbedByStudentNameWithSoGoCheck(student)], ephemeral: true }); }
        else
            await interaction.reply({ content: "Nenašli jsme tě :/ Napiš adminovi (**/ticket**)", ephemeral: true });
        return;
    } else if (!await interaction.member.roles.cache.has('1045859234354581605')) {
        await interaction.reply({ content: "Toto je jen pro adminy ty zmetku!" });
        return;
    }
    if (jmeno && jmeno.length > 25) { interaction.reply({ content: "Délka jména může být max 25 znaků!", ephemeral: true }); return; }
    //--------------------- Actually to spravování žáků  ---------------------
    if (create) {
        const jmenoBetr = await jmeno.replace(/[,./;]/g, " ").replace(/\s{2,}/g, ' ').toString().trim().toLowerCase().normalize("NFD").replace(/[\u0300-\u036f]/g, "");
        if (!jmenoBetr) {
            await interaction.reply({ content: "Zadej jméno!", ephemeral: true });
            return;
        }
        if (studenti[jmenoBetr]) {
            await interaction.reply({ content: "Student již existuje!", ephemeral: true });
            return;
        }
        await interaction.reply({ content: "Vytvářím...", ephemeral: true });
        studenti[jmenoBetr] = {};
        writeFileSync("./jsons/studenti.json", JSON.stringify(studenti));
        await this.student_vyber(interaction, "a-0----" + jmenoBetr);

    } else {
        try {
            await interaction.update({ content: "Hledám...", ephemeral: true });
        } catch (error) {

        }
        try {
            await interaction.reply({ content: "Hledám...", ephemeral: true });
        } catch (error) {

        }
        await hledej(interaction, jmeno, 0, false, tridaHled, discord, zamereni, overwritten, legit);

    }
}
export async function student_plus(interaction, input) {
    input = await input.split("-");
    await interaction.update({ content: "Hledám...", ephemeral: true });
    await hledej(interaction, input[2], parseInt(input[1]) + 10, false);
}
export async function student_minus(interaction, input) {
    input = await input.split("-");
    var getTridy = false;
    if (input[3])
        getTridy = true;
    await interaction.update({ content: "Hledám...", ephemeral: true });
    await hledej(interaction, input[2], parseInt(input[1]) - 10, getTridy);
}
export async function student_vyber(interaction, input) {
    var jmeno;
    if (interaction.values)
        jmeno = interaction.values[0];
    else
        jmeno = await input.split("-")[5];
    await studentKartaUpdate(interaction, input, jmeno);
}
export async function student_upravit(interaction, input) {
    input = await input.split("-");
    var zmena = undefined;
    if (interaction.values)
        zmena = interaction.values[0];
    if (input[4])
        zmena = input[4];
    const modal = new ModalBuilder()
        .setCustomId('student_handle-' + JSON.stringify(input))
        .setTitle('Změna u studenta');
    const inputText = new TextInputBuilder()
        .setCustomId("inputText")
        .setLabel("Input pro " + zmena)
        .setStyle(TextInputStyle.Short)
        .setPlaceholder('Pro vymazání napiš "0"...')
        .setRequired(true);

    const firstActionRow = new ActionRowBuilder().addComponents(inputText);
    modal.addComponents(firstActionRow);
    await interaction.showModal(modal);
}
export async function student_handle(interaction, input) {
    input = await input.split("-");
    var inputText = undefined;
    if (interaction.fields)
        inputText = await interaction.fields.getTextInputValue('inputText');
    await changeStudentSetting(interaction, input[1], input[2], input[3], input[4], inputText);
}

function getEmbed(student) {
    var trida;
    var tridaString;
    if (studenti[student].trida) {
        tridaString = studenti[student].trida.toLowerCase().replace(".", "");
        trida = skupiny[tridaString].fulljmeno;
    }
    else trida = "nenalezeno";
    var zak;
    if (studenti[student].discord) zak = studenti[student].discord + "/" + student;
    else zak = studenti[student].fulljmeno + "/" + student;

    const post = new EmbedBuilder()
        .setColor(0x0099FF)
        .setDescription(
            `Žák: **${zak}**\nTřída: **${trida}**`
        )
    return post;
}

async function hledej(interaction, jmeno, posun, nacistTidy, hledanaTrida, jeNaDiscordu, hledaneZamereni, jeOverwritten, jeLegit) {
    var nalezeniStudenti = [];
    const keys = Object.keys(studenti);
    var user;

    if (/[0-9]/g.test(jmeno) && /[@<>]/g.test(jmeno)) {
        const user_id = await jmeno.replace(/[<>@]/g, "");
        user = await interaction.guild.members.cache.get(user_id);

    }
    for (var i = 0; i < keys.length; i++) {


        if (studenti[keys[i]] && (studenti[keys[i]].discord === jmeno || jmeno === keys[i]))
            nalezeniStudenti.push(keys[i]);
        else if (
            studenti[keys[i]]
            &&
            studenti[keys[i]].fulljmeno
            &&
            (jmeno == undefined || await studenti[keys[i]].fulljmeno.replace(/[,./;]/g, " ").replace(/\s{2,}/g, ' ').toString().trim().toLowerCase().normalize("NFD").replace(/[\u0300-\u036f]/g, "").includes(await jmeno.replace(/[,./;]/g, " ").replace(/\s{2,}/g, ' ').toString().trim().toLowerCase().normalize("NFD").replace(/[\u0300-\u036f]/g, "")))
            &&
            (jeNaDiscordu == undefined || (studenti[keys[i]].discord != undefined) == jeNaDiscordu)
            &&
            (jeOverwritten == undefined || (studenti[keys[i]].ow == true) == jeOverwritten)
            &&
            (jeLegit == undefined || studenti[keys[i]].checked == jeLegit)
            &&
            (hledanaTrida == undefined || (studenti[keys[i]].trida && await studenti[keys[i]].trida.replace(".", "").toLowerCase() === hledanaTrida.replace(".", "").toLowerCase()))
            &&
            (hledaneZamereni == undefined || (studenti[keys[i]].trida && await skupiny[studenti[keys[i]].trida.replace(".", "").toLowerCase()] && await skupiny[studenti[keys[i]].trida.replace(".", "").toLowerCase()].obor === hledaneZamereni))
        ) nalezeniStudenti.push(keys[i]);

        if (i == keys.length - 1) {

            if (nalezeniStudenti.length == 1) {
                await studentKartaUpdate(interaction, "a-" + posun + "-" + jmeno, nalezeniStudenti[0]);
                return;
            }
            if (nalezeniStudenti.length > 0) {
                var posts = [];
                var pocetDone = 0;
                var pocetCelkem = nalezeniStudenti.length;
                if (posun < 0 || posun > pocetCelkem) {
                    await interaction.editReply("Nalezeno " + pocetCelkem + " studentů");
                    return;
                }
                nalezeniStudenti = nalezeniStudenti.splice(posun, 10);
                nalezeniStudenti.forEach(async student => {
                    if (!studenti[student].discord && user) {
                        studenti[student].discord = await user.toString();
                        writeFileSync("./jsons/studenti.json", JSON.stringify(studenti));
                    }
                    if (!nacistTidy) posts.push(getEmbed(student));
                    else posts.push(await getEmbedByStudentNameWithSoGoCheck(student));
                    pocetDone++;

                    if (pocetDone === nalezeniStudenti.length) {
                        const components = [await getStudentActionRow(nalezeniStudenti, posun, jmeno),
                        new ActionRowBuilder().addComponents(
                            new ButtonBuilder()
                                .setCustomId('student_minus-'+JSON.stringify(input))
                                .setLabel("⬅️")
                                .setStyle(ButtonStyle.Primary),
                            new ButtonBuilder()
                                .setCustomId('student_minus-' + JSON.stringify(input))
                                .setLabel("🔄️")
                                .setStyle(ButtonStyle.Secondary),
                            new ButtonBuilder()
                                .setCustomId('student_plus-' + JSON.stringify(input))
                                .setLabel('➡️')
                                .setStyle(ButtonStyle.Primary),
                        )];
                        try {
                            await interaction.update("Hledám...");
                        } catch (error) {

                        }
                        await interaction.editReply({ content: "Nalezeno " + pocetCelkem + " studentů", components, embeds: posts, ephemeral: true }).catch(err => console.log(err));
                    }
                })

            } else {
                await interaction.editReply({ content: "Nikoho jsme nenašli... :/", components: [], embeds: [], ephemeral: true }).catch(err => console.log(err));
            }
        }
    }

}

async function getStudentActionRow(nalezeniStudenti, posun, jmeno) {
    var pocetDone = 0;
    var list = new StringSelectMenuBuilder()
        .setCustomId('student_vyber-'+JSON.stringify(input))
        .setPlaceholder('Nothing selected')
    await nalezeniStudenti.forEach(async student => {
        list.addOptions(
            {
                label: studenti[student].fulljmeno + "",
                description: student + "",
                value: student + "",
            })
        pocetDone++;
    })
    list.input = "gay";
    const row = new ActionRowBuilder()
        .addComponents(
            list
        );
    return row;

}

async function getEmbedByStudentNameWithSoGoCheck(student) {
    if (studenti[student].trida === undefined) {
        var tridaString = "null"
        var data = await got('http://mail.spseplzen.cz/SOGo/so/svitekj/Contacts/SPSE_adresy/' + student + '/view', { method: "GET", headers: config.sogo_headers }).catch(err => {
            if (err) tridaString = "null"
        });
        //console.log(data);
        try {
            data = await JSON.parse(data.body);
            try {
                tridaString = data.addresses[0].locality;
            } catch (err) {

            }
            if (tridaString != "null") {
                studenti[student].trida = tridaString;
                writeFileSync("./jsons/studenti.json", JSON.stringify(studenti));
            }
        } catch (error) {
            console.log("\n\nErr msg: \n" + error + "\n\n Inc. data: \n" + data + "\n\n At student.js -> getEmbedStudent() -> for " + student);
        }
        //console.log(string);


    }
    return getEmbedByStudentName(student);

}

async function getEmbedByStudentName(student) {
    const keysv = Object.keys(vyucujici);
    var pocetOznamkovani = 0;
    var zak;
    if (studenti[student].discord) zak = studenti[student].discord + " - " + student;
    else zak = studenti[student].fulljmeno + " - " + student;
    keysv.forEach(sv => {
        if (vyucujici[sv].hodnoceni && vyucujici[sv].hodnoceni.znamka[student]) pocetOznamkovani++;
    });
    var trida;
    if (studenti[student].trida) trida = studenti[student].trida.toLowerCase().replace(".", "");
    else trida = "null"
    const post = new EmbedBuilder()
        .setColor(0x0099FF)
        .setDescription(
            `Žák: **${zak}**
Legitness: **${studenti[student].checked}**
OverWriten? -> **${studenti[student].ow}**
Třída: **${skupiny[trida].fulljmeno}**
Oznámkováno **${pocetOznamkovani}** učitelů
Třídní učitel: **${skupiny[trida].ucitel}**
Kmenová učebna: **${skupiny[trida].ucebna}**
Obor: **${skupiny[trida].obor}**`
        )
    return post;

}

async function getOptionsStudent(jmeno, input) {
    var list = new StringSelectMenuBuilder()
        .setCustomId('student_upravit-' + JSON.stringify(input))
        .setPlaceholder('Nothing selected')
        .addOptions(
            {
                label: "Jméno",
                description: "Jméno a příjmení studenta",
                value: "fullname",
            })
        .addOptions(
            {
                label: "Overwrite",
                description: "Bot bude ignorovat, když uživatel na Novellu nebude",
                value: "ow",
            })
        .addOptions(
            {
                label: "Třída",
                description: "Třída do které žák chodí",
                value: "trida",
            })
        .addOptions(
            {
                label: "Discord",
                description: "Discord účet spárovaný s Novellem",
                value: "discord",
            })
    const row = new ActionRowBuilder()
        .addComponents(
            list
        );
    return row;
}

async function changeStudentSetting(interaction, posun, hledany, parm, studentNovell, value) {
    const keys = Object.keys(studenti);
    try {
        interaction.update("Pracuju...")
    } catch (error) {

    }
    var student;
    for (var i = 0; i < keys.length; i++) {
        if (studenti[studentNovell] != undefined) {
            student = studenti[studentNovell].discord;
            break;
        }
    }
    if (student) student = await interaction.guild.members.fetch(student.replace(/[@<>]/g, ""));
    /* if (parm === "discord") if (await interaction.guild.members.cache.get(value.replace(/[<>@]/g,"")) || value === "0") {
         if (value === "0") {
             //reset
             studenti[studentNovell].discord = undefined;
             fs.writeFileSync("./jsons/studenti.json", JSON.stringify(studenti));
             await interaction.editReply({ content: "U " + jmeno + " byl odstraněn " + parm, ephemeral: true }).catch(err => console.log(err));
         } else {
             studenti[studentNovell].discord = value;
             fs.writeFileSync("./jsons/studenti.json", JSON.stringify(studenti));
             await interaction.editReply({ content: "U " + jmeno + " byl zmněněn " + parm + " na " + value, ephemeral: true }).catch(err => console.log(err));
             return;
         }
     }
     else { await interaction.editReply("Zadaný discord nebyl validný!").catch(err => console.log(err)); return; }
 */
    //trida handeling-------------
    if (parm === "trida") if (value === "0" || skupiny[value.toLowerCase().replace(".", "")]) if (value === "0") {
        //reset
        if (studenti[studentNovell].trida) {
            const trida = await interaction.guild.roles.cache.find(role => role.name === studenti[studentNovell].trida);
            const trida_admin = await interaction.guild.roles.cache.find(role => role.name === studenti[studentNovell].trida + "_admin");
            if (trida != undefined) {
                await student.roles.remove(trida);
            }
            if (trida_admin != undefined) {
                await student.roles.remove(trida_admin);
            }
        }
        studenti[studentNovell].trida = undefined;
        writeFileSync("./jsons/studenti.json", JSON.stringify(studenti));
        await studentKartaUpdate(interaction, "a-" + posun + "-" + hledany, studentNovell);
        return;
    } else {

        //odstranit staré role třídy
        if (studenti[studentNovell].trida) {
            const trida = await interaction.guild.roles.cache.find(role => role.name === studenti[studentNovell].trida);
            const trida_admin = await interaction.guild.roles.cache.find(role => role.name === studenti[studentNovell].trida + "_admin");
            if (trida != undefined) {
                if (student) await student.roles.remove(trida);
            }
            if (trida_admin != undefined) {
                if (student) await student.roles.remove(trida_admin);
            }
        }

        //dát nové role třídy
        const trida = await interaction.guild.roles.cache.find(role => role.name === value);
        if (!trida && student) {
            await interaction.editReply("Vytvářím role...").catch(err => console.log(err));
            const role = await interaction.guild.roles.create({
                name: value,
                color: [0, 255, 40],
                reason: 'Role pro uživatele třídy',
                hoist: true,
                mentionable: true
            }).catch(console.error);
            const admin = await interaction.guild.roles.create({
                name: value + "_admin",
                color: 'AQUA',
                reason: 'Role pro admina třídy'
            }).catch(console.error);
            const pO = [{
                type: "role",
                id: role.id,
                allow: [PermissionFlagsBits.ViewChannel,
                PermissionFlagsBits.SendMessages],
            }, {
                type: "role",
                id: '1037064627571392553',
                deny: [PermissionFlagsBits.ViewChannel],
            }, {
                type: "role",
                id: admin.id,
                allow: [PermissionFlagsBits.ManageRoles,
                PermissionFlagsBits.ManageEmojisAndStickers,
                PermissionFlagsBits.ManageChannels],
            }];
            await interaction.editReply("Vytvářím kanály...").catch(err => console.log(err));
            const category = await interaction.guild.channels.create({
                name: value,
                type: ChannelType.GuildCategory,
                permissionOverwrites: pO,
            });
            const text = await interaction.guild.channels.create({
                name: "school chat",
                type: ChannelType.GuildText,
                permissionOverwrites: pO,
                parent: category,
            });
            const voice = await interaction.guild.channels.create({
                name: "general chat",
                type: ChannelType.GuildVoice,
                permissionOverwrites: pO,
                parent: category,
            });
            roles[role.id] = category.id;
            if (student) await student.roles.add(role);
            if (student) await student.roles.add(admin);
            writeFileSync("./jsons/roles.json", JSON.stringify(roles));
            await interaction.editReply("Třída updatnuta na " + value + " a příslušné kanály vytvořeny.").catch(err => console.log(err));
            await text.send("Vítej " + student.toString() + " v " + value + "!\n seš tu jako první, takže máš právo na:\n  -vytváření kanálů ve vaší kategorii\n  -**/backup** pro vytvoření zálohy z chatu");
            studenti[studentNovell].trida = value;
        } else {
            if (student) await student.roles.add(trida);
            studenti[studentNovell].trida = value;
        }


        writeFileSync("./jsons/studenti.json", JSON.stringify(studenti));
        await studentKartaUpdate(interaction, "a-" + posun + "-" + hledany, studentNovell);
        return;
    }
    else { await interaction.update("Zadaná třída nebyla validní!").catch(err => console.log(err)); return; }

    //jmeno handeling ----------------------------------
    if (parm === "fullname") {
        if (!value) {
            await interaction.update("Zadaný jméno nebylo validní!");
            return;
        }
        if (value === "0") {
            studenti[studentNovell].fulljmeno = undefined;
            if (student) await student.setNickname(null);
            

        } else {
            studenti[studentNovell].fulljmeno = value;
            if (student) await student.setNickname(value);
        }

        await studentKartaUpdate(interaction, "a-" + posun + "-" + hledany, studentNovell);
        return;
    }

    //delete handel----------------------
    if (parm === "delete") {
        if (value !== "0") {
            await interaction.update("Odstranění nebylo potvrzeno!").catch(err => console.log(err));
            return;
        }
        if (student) await student.roles.remove(student.roles.cache);
        if (student && student.id != "543459815746306055") await student.setNickname(null);
        delete studenti[studentNovell];
        writeFileSync("./jsons/studenti.json", JSON.stringify(studenti));
        await interaction.update("Uživatel odstraněn!").catch(err => err);
        await interaction.editReply("Uživatel odstraněn!").catch(err => err);
        setTimeout(async () => {
            await hledej(interaction, hledany, posun, false);
            return;
        }, 1500)
    }

    //ow handel-------------------------
    if (parm === "ow") {
        if (value === "0") {
            if (studenti[studentNovell].ow) studenti[studentNovell].ow = false;
            else studenti[studentNovell].ow = true;
            writeFileSync("./jsons/studenti.json", JSON.stringify(studenti));
            await studentKartaUpdate(interaction, "a-" + posun + "-" + hledany, studentNovell);
        } else await interaction.editReply("Pro přepnutí potvrď pomocí \"0\"").catch(err => (err));
    }

    //reset handle --------------------------------
    if (parm === "reset") {
        if (value !== "0") {
            await interaction.update("Odstranění nebylo potvrzeno! - potvrď pomocí \"0\"").catch(err => console.log(err));
            return;
        }
        //vymazani
        if (student) await student.roles.remove(student.roles.cache);
        if (student && student.id != "543459815746306055") await student.setNickname(null);
        studenti[studentNovell] = {};
        if (student) await student.roles.add("1045791758128255017");
        if (student) studenti[studentNovell].discord = await student.toString();
        writeFileSync("./jsons/studenti.json", JSON.stringify(studenti));
        try {
            var data = await got('http://mail.spseplzen.cz/SOGo/so/svitekj/Contacts/SPSE_adresy/' + studentNovell + '/view', { method: "GET", headers: config.sogo_headers });
            //console.log(data);
            try {
                data = await JSON.parse(data.body);
            } catch (error) {
                console.log(error);
                console.log(data);
            }
            //jmeno handle
            studenti[studentNovell].fulljmeno = data.c_cn;
            if (student && student.id != "543459815746306055") await student.setNickname(studenti[studentNovell].fulljmeno);
            //trida handle
            const tridaString = data.addresses[0].locality;
            studenti[studentNovell].trida = tridaString;
            if (await skupiny[tridaString.toLowerCase().replace(".", "")] === undefined) {
                return;
            }
            const trida = await interaction.guild.roles.cache.find(role => role.name === tridaString);
            if (student) if (trida != undefined) {
                //kdyz je trida
                await student.roles.add(trida);
            } else {
                //kdyz neni jeste trida
                await interaction.editReply("Vytvářím role...").catch(err => console.log(err));
                const role = await interaction.guild.roles.create({
                    name: tridaString,
                    color: [0, 255, 40],
                    reason: 'Role pro uživatele třídy',
                    hoist: true,
                    mentionable: true
                }).catch(console.error);
                const admin = await interaction.guild.roles.create({
                    name: tridaString + "_admin",
                    color: 'AQUA',
                    reason: 'Role pro admina třídy'
                }).catch(console.error);
                const pO = [{
                    type: "role",
                    id: role.id,
                    allow: [PermissionFlagsBits.ViewChannel,
                    PermissionFlagsBits.SendMessages],
                }, {
                    type: "role",
                    id: '1037064627571392553',
                    deny: [PermissionFlagsBits.ViewChannel],
                }, {
                    type: "role",
                    id: admin.id,
                    allow: [PermissionFlagsBits.ManageRoles,
                    PermissionFlagsBits.ManageEmojisAndStickers,
                    PermissionFlagsBits.ManageChannels],
                }];
                await interaction.editReply("Vytvářím kanály...").catch(err => console.log(err));
                const category = await interaction.guild.channels.create({
                    name: tridaString,
                    type: ChannelType.GuildCategory,
                    permissionOverwrites: pO,
                });
                const text = await interaction.guild.channels.create({
                    name: "school chat",
                    type: ChannelType.GuildText,
                    permissionOverwrites: pO,
                    parent: category,
                });
                const voice = await interaction.guild.channels.create({
                    name: "general chat",
                    type: ChannelType.GuildVoice,
                    permissionOverwrites: pO,
                    parent: category,
                });
                await student.roles.add(role);
                await student.roles.add(admin);
                roles[role.id] = category.id;
                writeFileSync("./jsons/roles.json", JSON.stringify(roles));
                await text.send("Vítej " + interaction.user.toString() + " v " + tridaString + "!\n seš tu jako první, takže máš právo na:\n  -vytváření kanálů ve vaší kategorii\n  -**/backup** pro vytvoření zálohy z chatu");

            }
            //zamereni
            const zamereni = await skupiny[await tridaString.toLowerCase().replace(".", "")].obor.slice(0, 10);
            if (student) switch (zamereni) {
                case "18-20-M/01":
                    await student.roles.add("1050134787416920116");
                    break;
                case "26-41-M/01":
                    await student.roles.add("1050135125486211102");
                    break;
                case "78-42-M/01":
                    await student.roles.add("1050135113494691920");
                    break;

                default:
                    console.log("zaměření " + skupiny[await tridaString.toLowerCase().replace(".", "")].obor + " nebylo nalezeno");
                    break;
            }
            writeFileSync("./jsons/studenti.json", JSON.stringify(studenti));

        } catch (error) {
            console.log(error);
        }

        await interaction.update("Uživatel resetován!").catch(err => err);
        await interaction.editReply("Uživatel resetován!").catch(err => err);
        setTimeout(async () => {
            await hledej(interaction, hledany, posun, false);
            return;
        }, 1500)
    }

}

async function studentKartaUpdate(interaction, input, student) {
    input = input.split("-");
    const components = [
        await getOptionsStudent(student, input),
        new ActionRowBuilder().addComponents(
            new ButtonBuilder()
                .setCustomId('student_minus-' + JSON.stringify(input))
                .setLabel("⬅️ zpět")
                .setStyle(ButtonStyle.Primary),
            new ButtonBuilder()
                .setCustomId('student_upravit-' + JSON.stringify(input))
                .setLabel("🔄️ resetovat studenta")
                .setStyle(ButtonStyle.Secondary),
            new ButtonBuilder()
                .setCustomId('student_upravit-' + JSON.stringify(input))
                .setLabel("🚫 odstranit studenta")
                .setStyle(ButtonStyle.Danger)
        ),
    ]
    if (!studenti[student]) {
        try {
            await interaction.update({ content: "Nikoho jsme nenašli... :/", components: [], embeds: [], ephemeral: true });
        } catch (aee) { }
        await interaction.editReply({ content: "Nikoho jsme nenašli... :/", components: [], embeds: [], ephemeral: true })
    } else {
        const embeds = [await getEmbedByStudentNameWithSoGoCheck(student)]
        try {
            await interaction.update({
                content: "Konkrétní student:",
                embeds,
                components,
                ephemeral: true
            })
        } catch (aee) { }
        try {
            await interaction.editReply({
                content: "Konkrétní student:",
                embeds,
                components,
                ephemeral: true
            })
        } catch (aee) { }

    }
}