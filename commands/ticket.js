import { SlashCommandBuilder, PermissionFlagsBits, ChannelType } from 'discord.js';

export const data = new SlashCommandBuilder()
    .setName('ticket')
    .setDescription('Vytvoříš si ticket')
    //.setDefaultMemberPermissions(PermissionFlagsBits.CreatePrivateThreads)
    .setDMPermission(false)
    .addBooleanOption(option => option.setName('close_ticket')
        .setRequired(false)
        .setDescription("Chcete ticket uzavřít? (použijte v kanálu ticket)"));
export async function execute(interaction) {
    const close = await interaction.options.getBoolean('close_ticket');
    if (close) {
        if (await interaction.channel.parentId === "1045109819612332072") {
            await interaction.channel.setParent("1059803178071232612");
            await interaction.channel.setName(interaction.channel.name + "_closed");
            await interaction.channel.permissionOverwrites.edit(interaction.guild.roles.cache.get("1045791758128255017"), {
                "ViewChannel": false,
            });
            await interaction.reply("Ticket ukončen!");
        } else
            interaction.reply({ content: "Toto není ticket!", ephemeral: true });
    } else {
        await interaction.reply({ content: "Vytvářím ti ticket...", ephemeral: true }).catch(err => console.error(err));
        if (await interaction.guild.channels.cache.some(channel => channel.name === interaction.user.username + interaction.user.discriminator)) {
            await interaction.editReply({ content: "Už máš vytvořený ticket... vyřeš to tam!", ephemeral: true }).catch(err => console.log(err));
            return;
        } else {
            const channelTicket = await interaction.guild.channels.create({
                name: interaction.user.username + interaction.user.discriminator,
                type: ChannelType.GuildText,
                permissionOverwrites: [{
                    type: "user",
                    id: interaction.member.id,
                    allow: [PermissionFlagsBits.ViewChannel,
                    PermissionFlagsBits.SendMessages],
                }, {
                    type: "role",
                    id: '1037064627571392553',
                    deny: [PermissionFlagsBits.ViewChannel],
                }],
                parent: await interaction.guild.channels.fetch("1045109819612332072"),
            });
            const channel = await interaction.guild.channels.fetch("1042153743665332225");

            await channel.send({ content: "Nový ticket " + channelTicket.toString() });
            await interaction.editReply({ content: "Ticket byl vytvořen!", ephemeral: true }).catch(err => console.log(err));
        }
    }
}
