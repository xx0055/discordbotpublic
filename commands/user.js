import { SlashCommandBuilder, PermissionFlagsBits, EmbedBuilder } from 'discord.js';
import studenti from "../jsons/studenti.json" assert { type: "json" };
import skupiny from "../jsons/skupiny.json" assert { type: "json" };
import vyucujici from "../jsons/vyucujici.json" assert { type: "json" };

export const data = new SlashCommandBuilder()
	.setName('user')
	.setDefaultMemberPermissions(PermissionFlagsBits.CreatePrivateThreads)
	.setDMPermission(false)
	.setDescription('Vypíše ti informace které o tobě máme.');
export async function execute(interaction) {
	const keyss = Object.keys(studenti);
	const keysv = Object.keys(vyucujici);
	var student;
	var trida;
	var pocetOznamkovani = 0;
	keyss.forEach(ss => {
		if (studenti[ss].discord === interaction.user.toString()) { student = ss; return; }
	});
	keysv.forEach(sv => {
		if (vyucujici[sv].hodnoceni && vyucujici[sv].hodnoceni.znamka[student])
			pocetOznamkovani++;
	});
	trida = studenti[student].trida.toLowerCase().replace(".", "");
	const post = new EmbedBuilder()
		.setColor(0x0099FF)
		.setDescription(
			`Žák: **${studenti[student].discord}**
Třída: **${skupiny[trida].fulljmeno}**
Oznámkováno **${pocetOznamkovani}** učitelů
Třídní učitel: **${skupiny[trida].ucitel}**
Kmenová učebna: **${skupiny[trida].ucebna}**
Obor: **${skupiny[trida].obor}**`
		);
	if (student) { await interaction.reply({ embeds: [post], ephemeral: true }); }
	else
		await interaction.reply({ content: "Nenašli jsme tě :/ Napiš adminovi (**/ticket**)", ephemeral: true });

}
