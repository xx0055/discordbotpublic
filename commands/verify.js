import { SlashCommandBuilder, PermissionFlagsBits, ChannelType } from 'discord.js';
import users from '../jsons/users.json' assert { type: "json" };
import { writeFileSync } from "node:fs";
import studenti from "../jsons/studenti.json" assert { type: "json" };
import config from "../jsons/config.json" assert { type: "json" };
import skupiny from "../jsons/skupiny.json" assert { type: "json" };
import roles from "../jsons/roles.json" assert { type: "json" };
import got from "got";

export const data = new SlashCommandBuilder()
    .setName('verify')
    .setDefaultMemberPermissions(PermissionFlagsBits.UseExternalStickers)
    .setDescription('Odemkne ti brány do světa prestiže')
    .addStringOption(option => option.setName('kod')
        .setDescription('Zadejte kód co ti přišel mailem.')
        .setRequired(true))
    .setDMPermission(false);
export async function execute(interaction) {
    //načítání promněných a ošetření vyjímek
    await interaction.reply({ content: "Very-fakuju tě...", ephemeral: true });
    const kod = await parseInt(interaction.options.getString("kod").replace(/([^0-9]+)/gi, ''));
    if (await interaction.member.roles.cache.get("1045791758128255017")) {
        await interaction.editReply({ content: "Hele už seš v systému.", ephemeral: true }).catch(err => console.log(err));
        return;
    }
    if (kod.length > 69) {
        await interaction.editReply({ content: "Maximální délka řetězce je 69!", ephemeral: true }).catch(err => console.log(err));
        return;
    }
    if (users[interaction.user.id] === kod) {
        await prihlaseno(interaction,users[interaction.member.id + "name"]);
        delete users[interaction.user.id];
        delete users[interaction.user.id + "name"];
        writeFileSync("./jsons/users.json", JSON.stringify(users));
    } else {
        await interaction.editReply({ content: "Sorry khámo, ale špatný kód... Možná ti vypršel, tak si vytvoř nový!", ephemeral: true }).catch(err => console.log(err));
    }
}

export async function prihlaseno(interaction,studentString){
    //actually když vše vyjde
    try {
        await interaction.member.roles.remove("1061938494399852596");
    } catch (error) { }
    await interaction.member.roles.add("1045791758128255017");
    const student = studenti[studentString];
    student.discord = await interaction.user.toString();
    writeFileSync("./jsons/studenti.json", JSON.stringify(studenti));
    try {
        var data = await got('http://mail.spseplzen.cz/SOGo/so/svitekj/Contacts/SPSE_adresy/' + studentString + '/view', { method: "GET", headers: config.sogo_headers });
        //console.log(data);
        try {
            data = await JSON.parse(data.body);
        } catch (error) {
            console.log(error);
            console.log(data);
        }

        const tridaString = data.addresses[0].locality;
        student.trida = tridaString;
        if (await skupiny[tridaString.toLowerCase().replace(".", "")] === undefined) {
            await interaction.editReply("Nenašli jsme tvou třídu, nahlaš toto správci! \n Ale vítej u nás! hehe :sweat_smile: ").catch(err => console.log(err));
            return;
        }
        const trida = await interaction.guild.roles.cache.find(role => role.name === tridaString);
        if (trida != undefined) {
            await interaction.member.roles.add(trida);
            await interaction.editReply("Vítej u nás a ve třídě: " + tridaString).catch(err => console.log(err));
        } else {
            await interaction.editReply("Vytvářím role...").catch(err => console.log(err));
            const role = await interaction.guild.roles.create({
                name: tridaString,
                color: [0, 255, 40],
                reason: 'Role pro uživatele třídy',
                hoist: true,
                mentionable: true
            }).catch(console.error);
            const admin = await interaction.guild.roles.create({
                name: tridaString + "_admin",
                color: 'AQUA',
                reason: 'Role pro admina třídy'
            }).catch(console.error);
            const pO = [{
                type: "role",
                id: role.id,
                allow: [PermissionFlagsBits.ViewChannel,
                PermissionFlagsBits.SendMessages],
            }, {
                type: "role",
                id: '1037064627571392553',
                deny: [PermissionFlagsBits.ViewChannel],
            }, {
                type: "role",
                id: admin.id,
                allow: [PermissionFlagsBits.ManageRoles,
                PermissionFlagsBits.ManageEmojisAndStickers,
                PermissionFlagsBits.ManageChannels],
            }];
            await interaction.editReply("Vytvářím kanály...").catch(err => console.log(err));
            const category = await interaction.guild.channels.create({
                name: tridaString,
                type: ChannelType.GuildCategory,
                permissionOverwrites: pO,
            });
            const text = await interaction.guild.channels.create({
                name: "school chat",
                type: ChannelType.GuildText,
                permissionOverwrites: pO,
                parent: category,
            });
            const voice = await interaction.guild.channels.create({
                name: "general chat",
                type: ChannelType.GuildVoice,
                permissionOverwrites: pO,
                parent: category,
            });
            await interaction.member.roles.add(role);
            await interaction.member.roles.add(admin);
            roles[role.id] = category.id;
            writeFileSync("./jsons/roles.json", JSON.stringify(roles));
            await interaction.editReply("Vítej u nás a ve třídě: " + tridaString).catch(err => console.log(err));
            await text.send("Vítej " + interaction.user.toString() + " v " + tridaString + "!\n seš tu jako první, takže máš právo na:\n  -vytváření kanálů ve vaší kategorii\n  -**/backup** pro vytvoření zálohy z chatu");

        }
        const zamereni = await skupiny[await tridaString.toLowerCase().replace(".", "")].obor.slice(0, 10);
        switch (zamereni) {
            case "18-20-M/01":
                await interaction.member.roles.add("1050134787416920116");
                break;
            case "26-41-M/01":
                await interaction.member.roles.add("1050135125486211102");
                break;
            case "78-42-M/01":
                await interaction.member.roles.add("1050135113494691920");
                break;

            default:
                console.log("zaměření " + skupiny[await tridaString.toLowerCase().replace(".", "")].obor + " nebylo nalezeno");
                break;
        }
        if (interaction.member.id != interaction.guild.ownerId)
            await interaction.member.setNickname(student.fulljmeno);
        writeFileSync("./jsons/studenti.json", JSON.stringify(studenti));

    } catch (error) {
        console.log(error);
    }
}
