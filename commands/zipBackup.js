import { SlashCommandBuilder, PermissionFlagsBits } from 'discord.js';
import { existsSync, mkdirSync, createWriteStream } from 'node:fs';
import archiver from "archiver";

export const data = new SlashCommandBuilder()
  .setName('zipbackup')
  .setDescription('Zazipuje vaše zálohy - řekni si mi pak o kopii')
  .setDMPermission(false)
  .setDefaultMemberPermissions(PermissionFlagsBits.ManageChannels);
export async function execute(interaction) {
  await interaction.reply("Vytvářím .zip soubor...");
  const channel = interaction.channel;
  var fileName = "";
  if (channel.parent == null)
    fileName = channel.parent.name + "-" + channel.parent.id;
  else
    fileName = channel.parent.name + "-" + channel.parent.id;
  const dir = "./backups_zips/";
  const backupDir = "./backups/" + fileName + "/";
  if (!existsSync("./backups_zips/")) {
    mkdirSync("./backups_zips/");
  }
  if (!existsSync(dir)) {
    mkdirSync(dir);
  }
  var output = createWriteStream(dir + fileName + ".zip");
  var archive = archiver('zip', { zlib: { level: 9 } });
  output.on('close', async () => {
    console.log(archive.pointer() + ' total bytes');
    console.log('Backup kategorie ' + fileName + " byl vytvořen.");
    await interaction.editReply("Zip soubor vytvořen").catch(err => console.log(err));
  });
  archive.on('warning', function (err) {
    if (err.code === 'ENOENT') {
    } else {
      // throw error
      throw err;
    }
  });

  // good practice to catch this error explicitly
  archive.on('error', function (err) {
    throw err;
  });
  archive.pipe(output);
  archive.directory(backupDir, false);
  await archive.finalize();

}