export async function hledej(kde, co) {
    if (typeof co === "number" && typeof kde === "number") {
        if (kde - co == 0)
            return 100;
        else if (kde - co > 0)
            return (100 / Math.ceil(kde - co));
        else
            return (100 / Math.ceil(co - kde));
    } else if (typeof co === "string" && typeof kde === "string") {
        if (co === kde)
            return 999999999999999;
        else {
            var body = 0;
            const arrCo = co.replace(/[,./;]/g, " ").replace(/\s{2,}/g, ' ').toString().trim().toLowerCase().normalize("NFD").replace(/[\u0300-\u036f]/g, "").split(" ");
            const arrKde = kde.replace(/[,./;]/g, " ").replace(/\s{2,}/g, ' ').toString().trim().toLowerCase().normalize("NFD").replace(/[\u0300-\u036f]/g, "").split(" ");
            arrKde.forEach(async (kdeSl) => {
                if (arrCo.some(coSl => coSl === kdeSl))
                    body += 100;
                else {
                    var bodySlovoMax = 0;
                    const arrKdeSl = kdeSl.split("");
                    arrCo.forEach(coSl => {
                        var shodnaPismena = 0;
                        arrKdeSl.forEach(pismeno => {
                            if (coSl.includes(pismeno))
                                shodnaPismena += 1;
                        });
                        var bodySlovo = 100 * shodnaPismena / arrKdeSl.length;
                        if (bodySlovo > bodySlovoMax)
                            bodySlovoMax = bodySlovo;
                    });
                    body += bodySlovoMax;
                }
            });
            return Math.round(body / arrKde.length);
        }
    } else
        throw "špatný datatype, podporujeme jen number a string, oba musí být stejné";
}
