import { writeFileSync } from 'node:fs';
import studenti from "../jsons/studenti.json" assert { type: "json" };
import skupiny from "../jsons/skupiny.json" assert { type: "json" };
import { client } from '../app.js';



export async function checkStudent(studentString, sogoData) {
    const student = studenti[studentString];
    if (!student) return;
    student.fulljmeno = sogoData.c_cn;
    student.trida = sogoData.addresses[0].locality;
    if (student.discord) {
        var user;
        try {
            user = await (await client.guilds.fetch("1037064627571392553")).members.fetch(student.discord.replace("<@", "").replace(">", ""));
        } catch (error) {
            delete student.trida;
            delete student.discord;
            return;
        }
        await user.setNickname(student.fulljmeno);
        if (undefined != await user.roles.cache.get("1045791758128255017")) {
            const defaultSkupina = await (await client.guilds.fetch("1037064627571392553")).roles.cache.get("1045791758128255017");
            if (defaultSkupina && !user.roles.cache.get("1045791758128255017")) {
                await user.roles.add(defaultSkupina);
            }
        }
        const tridaRole = await (await client.guilds.fetch("1037064627571392553")).roles.cache.find(role => role.name === student.trida);
        if (tridaRole && !user.roles.cache.find(role => role.name === student.trida)) {
            await user.roles.add(tridaRole);
        }
        
        if (skupiny[await student.trida.toLowerCase().replace(".", "")].obor && user) {
            const zamereni = await skupiny[await student.trida.toLowerCase().replace(".", "")].obor.slice(0, 10);
            switch (zamereni) {
                case "18-20-M/01":
                    if(!await user.roles.cache.get("1050134787416920116"))await user.roles.add("1050134787416920116");
                    break;
                case "26-41-M/01":
                    if(!await user.roles.cache.get("1050135125486211102"))await user.roles.add("1050135125486211102");
                    break;
                case "78-42-M/01":
                    if(!await user.roles.cache.get("1050135113494691920"))await user.roles.add("1050135113494691920");
                    break;

                default:
                    console.log("zaměření " + skupiny[await tridaString.toLowerCase().replace(".", "")].obor + " nebylo nalezeno");
                    break;
            }
            writeFileSync("./jsons/studenti.json", JSON.stringify(studenti));

        }
        
    }
}
