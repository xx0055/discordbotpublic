import config from '../jsons/config.json' assert { type: "json" };
import changesNovell from "../jsons/changesNovell.json" assert { type: "json" };
import { writeFileSync } from 'node:fs';
import vyucujici from "../jsons/vyucujici.json" assert { type: "json" };
import skupiny from "../jsons/skupiny.json" assert { type: "json" };
import studenti from "../jsons/studenti.json" assert { type: "json" };
import got from "got";
import { client } from '../app.js';
import {checkStudent} from './checkStudent.js';



export async function updateNovellChannel(novellUpdateChannel, fullCheck) {
	await novellUpdateChannel.messages.fetch();
	await novellUpdateChannel.messages.cache.forEach(async (message) => {
		await message.delete().catch(err => {
			if (err.rawError.code != 10008)
				console.log(err);
		});
	});
	var data = await got("https://mail.spseplzen.cz/SOGo/so/svitekj/Contacts/allContactSearch?priority=gcs&search=.cz", { method: "GET", headers: config.sogo_headers });
	try {
		changesNovell.added = {
			tridy: {},
			ucitele: {},
			studenti: {}
		};
		changesNovell.removed = {
			tridy: [],
			ucitele: [],
			studenti: []
		};

		var msgT = "**Třídy:**\n";
		var msgS = "**Studenti:**\n";
		var msgU = "**Učitelé:**\n";
		data = await JSON.parse(data.body);
		await Object.keys(skupiny).forEach(async (skupina) => {
			if (skupiny[skupina])
				skupiny[skupina].checked = false;
		});
		await Object.keys(studenti).forEach(async (student) => {
			if (studenti[student])
				studenti[student].checked = false;
		});
		await Object.keys(vyucujici).forEach(async (ucitel) => {
			if (vyucujici[ucitel])
				vyucujici[ucitel].checked = false;
		});
		await data.contacts.forEach(async (kontakt) => {

			if (!kontakt.note) {
				//skupiny
				if (skupiny[kontakt.id]) {
					skupiny[kontakt.id].checked = true;
				} else if (await /[0-9]/.test(kontakt.id)) {
					var newObject = {};
					newObject.fulljmeno = await kontakt.id.substring(0, 1) + "." + await kontakt.id.substring(1);
					newObject.checked = true;
					if (msgT.length > 1900) {
						await novellUpdateChannel.send(msgT);
						msgT = "**Třídy:**\n";
					}
					changesNovell.added.tridy[kontakt.id] = newObject;
					msgT += ("[ 🟢 NOVELL Plus ] - " + newObject.fulljmeno + " se z formovalo\n");
				}

			} else if (kontakt.note.includes("studující")) {
				//studenti
				if (studenti[kontakt.id]) {
					studenti[kontakt.id].checked = true;
				} else {
					var newObject = {};
					newObject.fulljmeno = kontakt.fn;
					newObject.checked = true;
					changesNovell.added.studenti[kontakt.id] = newObject;
					if (msgS.length > 1900) {
						await novellUpdateChannel.send(msgS);
						msgS = "**Studenti:**\n";
					}
					msgS += ("[ 🟢 NOVELL Plus ] - " + newObject.fulljmeno + " se spawnl\n");
				}

			} else if (kontakt.c_component == "vcard" && kontakt.fn) {
				//vyucujici
				if (vyucujici[kontakt.id]) {
					vyucujici[kontakt.id].checked = true;

				} else {
					var newObject = {};
					newObject.fulljmeno = kontakt.fn;
					newObject.cislo = kontakt.c_telephonenumber;
					newObject.checked = true;
					changesNovell.added.ucitele[kontakt.id] = newObject;
					if (msgU.length > 1900) {
						await novellUpdateChannel.send(msgU);
						msgU = "**Učitelé:**\n";
					}
					msgU += ("[ 🟢 NOVELL Plus ] - " + newObject.fulljmeno + " se spawnl\n");

				}
			}
		});
		await Object.keys(skupiny).forEach(async (skupina) => {
			if (skupiny[skupina].checked === false && skupiny[skupina].ow !== true) {
				if (msgT.length > 1900) {
					await novellUpdateChannel.send(msgT);
					msgT = "**Třídy:**\n";
				}
				await changesNovell.removed.tridy.push(skupina);
				msgT += ("[ 🔴 NOVELL Minus ] - " + skupina + " se rozpadla\n");
			}
		});
		await (await Object.keys(studenti)).forEach(async (student) => {
			if (studenti[student].checked === false && studenti[student].ow !== true) {
				if (msgS.length > 1900) {
					await novellUpdateChannel.send(msgS);
					msgS = "**Studenti:**\n";
				}
				await changesNovell.removed.studenti.push(student);
				msgS += ("[ 🔴 NOVELL Minus ] - " + student + " odešel ze školy\n");
			}
			if (fullCheck && studenti[student].checked && studenti[student].discord && !studenti[student].ow) {
				if (msgS.length > 1900) {
					await novellUpdateChannel.send(msgS);
					msgS = "**Studenti:**\n";
				}
				var data = await got('http://mail.spseplzen.cz/SOGo/so/svitekj/Contacts/SPSE_adresy/' + student + '/view', { method: "GET", headers: config.sogo_headers });
				try {
					data = await JSON.parse(data.body);
					const tridaString = data.addresses[0].locality;
					if (tridaString != studenti[student].trida) {
						const studentObject = studenti[student];
						studentObject.trida = tridaString;
						changesNovell.added.studenti[student] = studentObject;
						writeFileSync("./jsons/changesNovell.json", JSON.stringify(changesNovell));
						await novellUpdateChannel.send("[ 🟡 NOVELL Změna ] - " + student + " změna ve třídě\n");
					}
					try {
						await checkStudent(student,data);
					} catch (error) {
						console.error(error);
					}
					
				} catch (error) {
					await novellUpdateChannel.send("[ 💀 NOVELL Chyba ] - " + student + " nenašli jsme třídu\n");
				}
				
			}
			var isOnServer = false;
			try {
				if (await (await client.guilds.fetch("1037064627571392553")).members.fetch(studenti[student].discord.replace("<@", "").replace(">", "")))
					isOnServer = true;
			} catch (error) {
			}
			if (studenti[student].trida && !studenti[student].ow && !isOnServer) {
				studenti[student].trida = undefined;
			}
		});
		await Object.keys(vyucujici).forEach(async (ucitel) => {
			if (vyucujici[ucitel].checked === false && vyucujici[ucitel].ow !== true) {
				if (msgU.length > 1900) {
					await novellUpdateChannel.send(msgU);
					msgU = "**Učitelé:**\n";
				}
				await changesNovell.removed.ucitele.push(ucitel);
				msgU += ("[ 🔴 NOVELL Minus ] - " + ucitel + " odešel ze školy\n");
			}
		});
		writeFileSync("./jsons/changesNovell.json", JSON.stringify(changesNovell));
		writeFileSync("./jsons/studenti.json", JSON.stringify(studenti));
		if (msgU != "**Učitelé:**\n")
			await novellUpdateChannel.send(msgU);
		if (msgS != "**Studenti:**\n")
			await novellUpdateChannel.send(msgS);
		if (msgT != "**Třídy:**\n")
			await novellUpdateChannel.send(msgT);
	} catch (error) {
		console.error(error);
	}
}
